import observer.event.AirHumidityEvent;
import observer.event.TemperatureEvent;
import observer.model.AirHumidity;
import observer.model.Temperature;
import observer.eventlistner.WeatherSensor;
import observer.observer.Computer;
import observer.observer.Device;
import observer.publisher.System;

public class Main {

    public static void main(String[] arg){

        Temperature temperature = new Temperature(21);
        TemperatureEvent temperatureEvent = new TemperatureEvent(temperature);

        AirHumidity airHumidity = new AirHumidity(10);
        AirHumidityEvent airHumidityEvent = new AirHumidityEvent(airHumidity);

        System system = new System();
        system.addObserver(new Device());
        system.addObserver(new Computer());

        WeatherSensor weatherSensor = new WeatherSensor(system);
        WeatherSensor.register(temperatureEvent, "onTemperatureChange");
        WeatherSensor.register(airHumidityEvent, "onAirHumidityChange");
        weatherSensor.executeEvent(temperatureEvent);
        weatherSensor.executeEvent(airHumidityEvent);
    }
}
