package observer.eventlistner;

import observer.event.AirHumidityEvent;
import observer.event.Event;
import observer.event.TemperatureEvent;
import observer.model.Notification;
import observer.model.Registered;
import observer.publisher.System;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class WeatherSensor implements Sensor{

    private System system;

    public WeatherSensor(System system) {
        this.system = system;
    }

    private static List<Registered> registered = new ArrayList<>();

    public static void register(Event event, String process)
    {
        Registered register = new Registered(event, process);
        registered.add(register);
    }

    private void onTemperatureChange(TemperatureEvent temperatureEvent)
    {
        system.notifyObservers(new Notification("Temperature change to: ",""+temperatureEvent.getTemperature().getDegrees()));
    }

    private void onAirHumidityChange(AirHumidityEvent airHumidityEvent)
    {
        system.notifyObservers(new Notification("Air humidity change to: ",""+airHumidityEvent.getAirHumidity().getHumidity()));
    }

    public void executeEvent(Event event)
    {
        for (Registered registered : registered) {

            if (registered.getEvent().equals(event)) {

                Method method = null;

                Class<? extends WeatherSensor> weatherSensor = this.getClass();
                try {
                    method = weatherSensor.getDeclaredMethod(registered.getProcess(), event.getClass());
                    method.invoke(this, event);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
