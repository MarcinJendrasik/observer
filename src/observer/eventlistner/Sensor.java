package observer.eventlistner;

import observer.event.Event;

public interface Sensor {

    void executeEvent(Event event);
}
