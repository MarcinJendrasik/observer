package observer.observer;

import observer.model.Notification;

public class Computer implements Observer {

    @Override
    public void update(Notification notification) {
        System.out.println(notification.getSubject() + notification.getDescription());
    }
}
