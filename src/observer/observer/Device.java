package observer.observer;

import observer.model.Notification;

public class Device implements Observer {

    @Override
    public void update(Notification  notification) {
        System.out.println(notification.getSubject() + notification.getDescription());
    }
}
