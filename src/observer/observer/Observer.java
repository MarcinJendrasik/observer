package observer.observer;

import observer.model.Notification;

public interface Observer {

    public void update(Notification notification);
}
