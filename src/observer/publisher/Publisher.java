package observer.publisher;

import observer.model.Notification;
import observer.observer.Observer;

import java.util.List;

public interface Publisher {

    public void addObserver(Observer observer);
    public void removeObserver(Observer observer);
    public void notifyObservers(Notification notification);
}
