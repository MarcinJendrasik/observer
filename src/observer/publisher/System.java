package observer.publisher;

import observer.model.Notification;
import observer.observer.Observer;

import java.util.ArrayList;
import java.util.List;

public class System implements Publisher{

    private List<Observer> observers = new ArrayList<>();

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(Notification notification) {

        for (Observer observer : observers) {
            observer.update(notification);
        }
    }
}
