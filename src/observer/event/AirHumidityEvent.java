package observer.event;

import observer.model.AirHumidity;

public class AirHumidityEvent implements Event {

    public static final String name = "AirHumidityEvent";
    private AirHumidity airHumidity;

    public AirHumidityEvent(AirHumidity airHumidity)
    {
        this.airHumidity = airHumidity;
    }

    public AirHumidity getAirHumidity() {
        return airHumidity;
    }
}
