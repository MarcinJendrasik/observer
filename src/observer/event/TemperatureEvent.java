package observer.event;

import observer.model.Temperature;

public class TemperatureEvent implements Event{

    public static final String name = "TemperatureEvent";
    private Temperature temperature;

    public TemperatureEvent(Temperature temperature)
    {
        this.temperature = temperature;
    }

    public Temperature getTemperature() {
        return temperature;
    }
}
