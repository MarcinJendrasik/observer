package observer.model;

import observer.event.Event;

public class Registered {

    private Event event;
    private String process;

    public Registered(Event event, String process)
    {
        this.event = event;
        this.process = process;
    }

    public Event getEvent() {
        return event;
    }

    public String getProcess() {
        return process;
    }
}
