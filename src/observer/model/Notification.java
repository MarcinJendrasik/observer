package observer.model;

public class Notification {

    private String subject;
    private String description;

    public Notification(String subject, String description)
    {
        this.subject = subject;
        this.description = description;
    }

    public String getSubject() {
        return subject;
    }

    public String getDescription() {
        return description;
    }
}
