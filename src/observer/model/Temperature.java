package observer.model;

public class Temperature {

    private int degrees;

    public Temperature(int degrees)
    {
        this.degrees = degrees;
    }

    public int getDegrees() {
        return degrees;
    }
}
