package observer.model;

public class AirHumidity {

    private int humidity;

    public AirHumidity(int humidity)
    {
        this.humidity = humidity;
    }

    public int getHumidity() {
        return humidity;
    }
}
